const mysql = require('mysql');
const connection = mysql.createConnection({

    host: "localhost",
    user: "root",
    password: "",
    database: "examen2par",

});

//VERIFICACION DE CONEXION 
connection.connect((error)=>{
if(error) {
console.log(error);
return;
}
console.log('Conectado a la bd');
});


//INICIAMOS EXPRESS
const express = require('express');
const apps = express();

apps.use(express.urlencoded({extended:false}));
apps.use(express.json());

//MOTOR DE APARIENCIAS
apps.set('view engine', 'ejs');

//LIBRERIAS DE EXPRESS REQUERIDAS
const req = require('express/lib/request');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');

apps.listen(3000, (req, res) => {
    console.log("SERVER RUNNING IN http://localhost:3000");
  });

  //RUTAS
apps.get("/", (req, res) => {
    res.render("form");
  });

  //METODO POST ----  INSERCIONES!!!
  apps.post("/", async (req, res) => {
    inserccion = req.body;
    connection.query("INSERT INTO clientes SET ?", {
      ced: inserccion.ci,
      nom: inserccion.name,
      dir: inserccion.dir,
      telef: inserccion.telef,
      mail: inserccion.mail,
    });
    res.render("form");
  });